import numpy as np
import math

arr = np.array([
    [0, 1],
    [0, 2]
])

def find_nearest(array):
    nearest = [10000000000000, 100000000000000]
    for i in array:
        if math.hypot(nearest[0], nearest[1]) > math.hypot(i[0], i[1]):
            temp = nearest
            nearest = i
            i = nearest
    return array


find_nearest(arr)