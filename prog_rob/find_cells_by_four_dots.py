import json
import pickle


def set_instruction_line(X, Y, Z, W=-141.126, P=177.26172234448893, R=-105.51779920732888):
    instruction_line = ''
    instruction_line += "Move J [*]  X) "
    instruction_line += str(round(X, 3))
    instruction_line += "  Y) "
    instruction_line += str(round(Y, 3))
    instruction_line += "   Z) "
    instruction_line += str(round(Z, 3))
    instruction_line += "   W) "
    instruction_line += str(round(W, 3))
    instruction_line += "   P) "
    instruction_line += str(round(P, 3))
    instruction_line += "   R) "
    instruction_line += str(round(R, 3))
    instruction_line += "    T) 201.5   Speed-50 Ad 50 As 0.2 Dd 50 Ds 0.2 $F"

    return instruction_line


def calc_cells(a1: str, a8: str, h8: str, h1: str, visio: str, chips_out: str, clock: str):
    dots = {

    }

    letter_list = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
    digits_list = list(range(1, 9))

    for letter in letter_list:
        for digit in digits_list:
            cell = letter + str(digit)
            dots[cell] = {
                'x': 0,
                'y': 0,
                'z': 0,
                'w': 0,
                'p': 0,
                'r': 0
            }


    x_index = a1.find("X) ")
    y_index = a1.find("Y) ")
    z_index = a1.find("Z) ")
    w_index = a1.find("W) ")
    p_index = a1.find("P) ")
    r_index = a1.find("R) ")
    t_index = a1.find("T) ")

    dots['a1']['x'] = float(a1[x_index + 3:y_index - 1])
    dots['a1']['y'] = float(a1[y_index + 3:z_index - 1])
    dots['a1']['z'] = float(a1[z_index + 3:w_index - 1])
    dots['a1']['w'] = float(a1[w_index + 3:p_index - 1])
    dots['a1']['p'] = float(a1[p_index + 3:r_index - 1])
    dots['a1']['r'] = float(a1[r_index + 3:t_index - 1])

    x_index = a8.find("X) ")
    y_index = a8.find("Y) ")
    z_index = a8.find("Z) ")
    w_index = a8.find("W) ")
    p_index = a8.find("P) ")
    r_index = a8.find("R) ")
    t_index = a8.find("T) ")

    dots['a8']['x'] = float(a8[x_index + 3:y_index - 1])
    dots['a8']['y'] = float(a8[y_index + 3:z_index - 1])
    dots['a8']['z'] = float(a8[z_index + 3:w_index - 1])
    dots['a8']['w'] = float(a8[w_index + 3:p_index - 1])
    dots['a8']['p'] = float(a8[p_index + 3:r_index - 1])
    dots['a8']['r'] = float(a8[r_index + 3:t_index - 1])

    x_index = h1.find("X) ")
    y_index = h1.find("Y) ")
    z_index = h1.find("Z) ")
    w_index = h1.find("W) ")
    p_index = h1.find("P) ")
    r_index = h1.find("R) ")
    t_index = h1.find("T) ")

    dots['h1']['x'] = float(h1[x_index + 3:y_index - 1])
    dots['h1']['y'] = float(h1[y_index + 3:z_index - 1])
    dots['h1']['z'] = float(h1[z_index + 3:w_index - 1])
    dots['h1']['w'] = float(h1[w_index + 3:p_index - 1])
    dots['h1']['p'] = float(h1[p_index + 3:r_index - 1])
    dots['h1']['r'] = float(h1[r_index + 3:t_index - 1])

    x_index = h8.find("X) ")
    y_index = h8.find("Y) ")
    z_index = h8.find("Z) ")
    w_index = h8.find("W) ")
    w_index = h8.find("W) ")
    p_index = h8.find("P) ")
    r_index = h8.find("R) ")
    t_index = h8.find("T) ")

    dots['h8']['x'] = float(h8[x_index + 3:y_index - 1])
    dots['h8']['y'] = float(h8[y_index + 3:z_index - 1])
    dots['h8']['z'] = float(h8[z_index + 3:w_index - 1])
    dots['h8']['w'] = float(h8[w_index + 3:p_index - 1])
    dots['h8']['p'] = float(h8[p_index + 3:r_index - 1])
    dots['h8']['r'] = float(h8[r_index + 3:t_index - 1])

    cell_offset = {
        'x': (dots['a8']['x'] - dots['a1']['x']) / (len(digits_list) - 1),
        'y': (dots['a8']['y'] - dots['a1']['y']) / (len(digits_list) - 1),
        'z': (dots['a8']['z'] - dots['a1']['z']) / (len(digits_list) - 1),
    }

    for i in range(2, 8):
        cell_cur = 'a'+str(i)
        cell_prev = 'a'+str(i-1)
        dots[cell_cur]['x'] = dots[cell_prev]['x'] + cell_offset['x']
        dots[cell_cur]['y'] = dots[cell_prev]['y'] + cell_offset['y']
        dots[cell_cur]['z'] = dots[cell_prev]['z'] + cell_offset['z']
        if i <= 4:
            dots[cell_cur]['w'] = dots['a1']['w']
            dots[cell_cur]['p'] = dots['a1']['p']
            dots[cell_cur]['r'] = dots['a1']['r']
        else:
            dots[cell_cur]['w'] = dots['a8']['w']
            dots[cell_cur]['p'] = dots['a8']['p']
            dots[cell_cur]['r'] = dots['a8']['r']

    cell_offset = {
        'x': (dots['h8']['x'] - dots['h1']['x']) / (len(digits_list) - 1),
        'y': (dots['h8']['y'] - dots['h1']['y']) / (len(digits_list) - 1),
        'z': (dots['h8']['z'] - dots['h1']['z']) / (len(digits_list) - 1),
    }

    for i in range(2, 8):
        cell_cur = 'h'+str(i)
        cell_prev = 'h'+str(i-1)
        dots[cell_cur]['x'] = dots[cell_prev]['x'] + cell_offset['x']
        dots[cell_cur]['y'] = dots[cell_prev]['y'] + cell_offset['y']
        dots[cell_cur]['z'] = dots[cell_prev]['z'] + cell_offset['z']
        if i <= 4:
            dots[cell_cur]['w'] = dots['h1']['w']
            dots[cell_cur]['p'] = dots['h1']['p']
            dots[cell_cur]['r'] = dots['h1']['r']
        else:
            dots[cell_cur]['w'] = dots['h8']['w']
            dots[cell_cur]['p'] = dots['h8']['p']
            dots[cell_cur]['r'] = dots['h8']['r']

    for i in range(1, 9):
        cell1 = 'h' + str(i)
        cell8 = 'a' + str(i)
        cell_offset = {
            'x': (dots[cell8]['x'] - dots[cell1]['x']) / (len(digits_list) - 1),
            'y': (dots[cell8]['y'] - dots[cell1]['y']) / (len(digits_list) - 1),
            'z': (dots[cell8]['z'] - dots[cell1]['z']) / (len(digits_list) - 1),
        }
        # print(cell_offset)
        for j in range(1, 7):
            cell_cur = letter_list[j] + str(i)
            cell_prev = letter_list[j-1] + str(i)
            # print(cell_cur, cell_prev)
            dots[cell_cur]['x'] = dots[cell_prev]['x'] - cell_offset['x']
            # print(dots[cell_prev]['x'] - cell_offset['x'])
            dots[cell_cur]['y'] = dots[cell_prev]['y'] - cell_offset['y']
            dots[cell_cur]['z'] = dots[cell_prev]['z'] - cell_offset['z']
            if i <= 4 and j < 5:
                dots[cell_cur]['w'] = dots['a1']['w']
                dots[cell_cur]['p'] = dots['a1']['p']
                dots[cell_cur]['r'] = dots['a1']['r']
            elif i > 4 and j < 5:
                dots[cell_cur]['w'] = dots['a8']['w']
                dots[cell_cur]['p'] = dots['a8']['p']
                dots[cell_cur]['r'] = dots['a8']['r']
            elif i <= 4 and j > 5:
                dots[cell_cur]['w'] = dots['h1']['w']
                dots[cell_cur]['p'] = dots['h1']['p']
                dots[cell_cur]['r'] = dots['h1']['r']
            else:
                dots[cell_cur]['w'] = dots['h8']['w']
                dots[cell_cur]['p'] = dots['h8']['p']
                dots[cell_cur]['r'] = dots['h8']['r']

    out_file = open("dots.json", "w")
    json.dump(dots, out_file, indent=3)
    out_file.close()

    instruction_set = ['##Start']

    for cell in dots.keys():
        instruction_set.append('##'+cell)
        instruction_set.append(set_instruction_line(dots[cell]['x'],
                                                    dots[cell]['y'],
                                                    dots[cell]['z'],
                                                    dots[cell]['w'],
                                                    dots[cell]['p'],
                                                    dots[cell]['r'],
                                                    ))

    instruction_set.append('##visio')
    instruction_set.append(visio)
    instruction_set.append('##chips_out')
    instruction_set.append(chips_out)
    instruction_set.append('##clock')
    instruction_set.append(clock)

    with open('programs/auto_calib', 'wb') as instruction_file:
        pickle.dump(instruction_set, instruction_file)

    print('auto_calib file saved!')
    print('OK!')

def new_auto_calib_from_file(filename='programs/calib_settings'):
    cells = []
    with open(filename, 'rb+') as f:
        koord = pickle.load(f)
    for k, v in enumerate(koord):
        if k % 2:
            temp = v
        if (not k % 2) and (k > 1):
            cells.append(v)

    a1 = cells[0]
    a8 = cells[1]
    h1 = cells[3]
    h8 = cells[2]
    visio = cells[4]
    chips_out = cells[5]
    clock = cells[6]

    calc_cells(a1, a8, h8, h1, visio, chips_out, clock)
